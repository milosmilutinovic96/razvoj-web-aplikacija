import React from 'react';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import { 
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import Login from './components/Login';
import Home from './components/Home';
import Register from './components/Register';
import MainAppBar from './components/MainAppBar';
import { ThemeProvider } from '@material-ui/styles';
import { Theme, createMuiTheme } from '@material-ui/core';
import { deepOrange } from '@material-ui/core/colors';


const theme: Theme = createMuiTheme({
  palette: {
    secondary: deepOrange
  }
});

const App: React.FC = () => {
  return (
    <>
      <CssBaseline />
      <Router>
        <ThemeProvider theme={theme}>
          <MainAppBar></MainAppBar>
          <Route path='/' exact component={Home} />
          <Route path='/auth/login' component={Login} />
          <Route path='/auth/register' component={Register} />
        </ThemeProvider>
      </Router>
    </>
  );
}

export default App;
