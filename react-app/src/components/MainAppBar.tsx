import React from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { AppBar, Toolbar, Link } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import logo from '../resources/bookerwise_logo.png';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        logo: {
            marginRight: theme.spacing(2),
            height: 40
        },
        spacer: {
            flexGrow: 1
        }
    })
);

const MainAppBar: React.FC = () => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <AppBar position='static' color='default'>
                <Toolbar>
                    <Link component={RouterLink} to='/'>
                        <img src={logo} alt="Logo" className={classes.logo} />
                    </Link>
                    <div className={classes.spacer}></div>
                    <Link component={RouterLink} to='/auth/login' underline='none'>
                        Login
                    </Link>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default MainAppBar;